import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by julienbelanger, p1088873 mike useni, p1130316 on 2019-02-03.
 */
public class MTFOmega {

    /**
     * Liste doublement chainee avec sentinels
     * Utile à la conservation de l'alphabet de MTF
     */
    public static class ListeDCS{

        private Noeud sentinelDroit ;
        private Noeud sentinelGauche;
        int max;

        static class Noeud{

            private Noeud prochain;
            private Noeud precedent;
            private int valeur;
            public int indice=-1;

            public Noeud(int valeur){ this.valeur = valeur; }
        }

        public void ajouteNoeud(Noeud noeud){
            Noeud dernier_noeud = sentinelDroit.precedent;
            noeud.prochain = sentinelDroit;
            noeud.precedent = dernier_noeud;
            sentinelDroit.precedent = noeud;
            dernier_noeud.prochain = noeud;
        }

        //Constructeur init par une valeur max
        public ListeDCS(int max) {

            this.max = max;
            this.sentinelDroit = new Noeud(-1);
            this.sentinelGauche = new Noeud(-1);

            sentinelGauche.prochain = sentinelDroit;
            //inutile dans la présente implémentation mais plus robuste
            sentinelGauche.precedent = null;
            sentinelDroit.precedent = sentinelGauche;
            sentinelDroit.prochain = null;

            // initialiser par une valeur max
            for (int i = 0; i <= max + 1; i++) {
                ajouteNoeud(new Noeud(i));
            }
        }

        //Module de recherche d'indice artificiel naïf
        public Noeud rechercheIndice(int valeur){
            Noeud present_noeud = sentinelGauche.prochain;

            int i = 0;
            while (present_noeud != sentinelDroit){
                if(present_noeud.valeur == valeur) break;
                present_noeud = present_noeud.prochain;
                i++;
            }

            if(i >= this.max+1){
                System.out.println("La valeur recherchée " +  i + " n'est pas dans l'alphabet");
                i = -1;
            }

            present_noeud.indice = i;

            return present_noeud;
        }

        //Module MTF opérationnel en O(1)
        public void MTF(Noeud noeud){

            //TODO: Utiliser une fonction enlever noeud et ajouter noeud (plus général)
            if(noeud.precedent != this.sentinelGauche){
                Noeud second_noeud = sentinelGauche.prochain;
                Noeud avant_noeud = noeud.precedent;
                Noeud apres_noeud = noeud.prochain;
                noeud.precedent = sentinelGauche;
                noeud.prochain = second_noeud;
                sentinelGauche.prochain = noeud;
                avant_noeud.prochain = apres_noeud;
                apres_noeud.precedent = avant_noeud;
            }
        }
    }

    //implémentation statique
    public static int encodeMTF(int i, ListeDCS alphabet){

        ListeDCS.Noeud nd = alphabet.rechercheIndice(i);
        alphabet.MTF(nd);

        return nd.indice;

    }

    //encodage récursif d'un integer à une valeur String binaire litérale
    public static String encodeBinaire(int valeur){

        if(valeur > 1){
            if(valeur % 2 == 0){return (encodeBinaire(valeur/2) + "0");}
            else if(valeur % 2 == 1){return encodeBinaire((valeur-1)/2)+"1";}
        }
        return ""+valeur;
    }

    //encodage récursif de l'encodage omega zero
    public static String encodeOmegaZero(int valeur){

        if(valeur > 0){
            String bin_val = encodeBinaire(valeur);
            return encodeOmegaZero(bin_val.length() - 1)+""+bin_val;
        }

        return "";
    }

    public static void writeBit(char i){

        System.out.write(i);

    }

    //encodage wrapped de encodage omega 0 (omega 1)
    public static void encodeOmega(int valeur){

        String omega = encodeOmegaZero(valeur)+"0";
        int ol = omega.length();
        //Gros boutiste (écriture gauche vers droite)
        for(int i = 0; i<16; i++){
            // pad avec 0 à droite (Gros boutiste)
            if(i >= ol){writeBit('0');}
            else{

                if(omega.charAt(i) == '0'){writeBit('0');}
                else{writeBit('1');}
            }
        }

    }

    public static void main(String[] args) {

        String nomFichier =  args[0]; //"resources/test.txt";

        //technique standard d'attrapage d'exception
        try {

            FileReader fr = new FileReader(nomFichier);
            //maximum tel que stipuler dans le devoir
            int max = 65536;
            //création d'une liste alphabet statique
            ListeDCS alphabet = new ListeDCS(max);


            int i;
            while ((i = fr.read())!= -1) {

                //stupide d'initialiser alphabet à chaque fois...
                encodeOmega(encodeMTF(i,alphabet));

            }

            fr.close();
        }
        catch (FileNotFoundException ex){System.out.println("Impossible de trouver le fichier: '" + nomFichier + "'");}
        catch(java.io.IOException ex) {System.out.println("Erreur d'ouverture du fichier: '" + nomFichier + "'");
        }
    }

}